# Usage

Usage instructions can be seen by running either of the following commands:

    php main.php

    php main.php help find

For example:

    php main.php find grid.txt bread


# Output

Sample output:

    $ php main.php find grid.txt bread
    The word occurs 4 time(s) in the grid.
    Starting at (0,1) in direction (0,1)
    Starting at (1,5) in direction (1,-1)
    Starting at (5,0) in direction (-1,0)
    Starting at (5,5) in direction (-1,-1)
