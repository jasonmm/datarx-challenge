<?php
declare(strict_types = 1);
namespace Jasonmm\DataRxChallenge\Commands;

use Jasonmm\DataRxChallenge\FindWord;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Class FindCommand handles the "find" command-line command.
 * @package Jasonmm\DataRxChallenge\Commands
 */
class FindCommand extends Command {

    protected function configure() {
        $this
            ->setName('find')
            ->setDescription('Find a given word in a wordsearch grid.')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'The file containing the wordsearch grid'
            )
            ->addArgument(
                'word',
                InputArgument::REQUIRED,
                'The word to search for in the wordsearch grid'
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output) {
        $grid = $this->getGridFromFile($input->getArgument('file'));

        $solver = new FindWord($grid, $input->getArgument('word'));
        $numOccurrences = $solver->Find();
        $occurrences = $solver->GetOccurrences();

        echo "The word occurs " . $numOccurrences . " time(s) in the grid.\n";
        foreach( $occurrences as $occurrence ) {
            echo "Starting at (" . $occurrence[0] . "," . $occurrence[1] . ") in direction (" . $occurrence[2][0] . "," . $occurrence[2][1] . ")\n";
        }
    }

    /**
     * Returns the two-dimensional array of characters from the given filename.
     *
     * @param string $filename
     *
     * @return array
     */
    private function getGridFromFile(string $filename) : array {
        $ret = [];

        $data = file_get_contents($filename);

        $rows = explode("\n", $data);
        foreach( $rows as $row ) {
            $row = trim($row);
            if( $row === '' ) {
                continue;
            }
            $row = str_split($row);
            $ret[] = $row;
        }

        return $ret;
    }
}
