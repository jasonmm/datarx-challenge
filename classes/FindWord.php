<?php
declare(strict_types = 1);
namespace Jasonmm\DataRxChallenge;

/**
 * Class FindWord finds a given word in a given wordsearch grid.
 * @package Jasonmm\DataRxChallenge
 */
class FindWord {
    /**
     * The directions of the compass.
     */
    const DIRECTIONS = [
        [1, 0],
        [1, 1],
        [0, 1],
        [-1, 1],
        [-1, 0],
        [-1, -1],
        [0, -1],
        [1, -1],
    ];

    private $grid = [];
    private $wordToFind = '';
    private $xDimension = 0;
    private $yDimension = 0;
    private $wordLen = 0;
    private $occurrences = [];

    /**
     * FindWord constructor.
     *
     * @param string[][] $grid the wordsearch grid
     * @param string $word     the word to search the grid for
     */
    public function __construct(array $grid, string $word) {
        $this->grid = $grid;
        $this->wordToFind = $word;
        $this->xDimension = count($this->grid[0]);
        $this->yDimension = count($this->grid);
        $this->wordLen = strlen($this->wordToFind);
    }

    /**
     * @return array
     */
    public function GetOccurrences() : array {
        return $this->occurrences;
    }

    /**
     * @return int
     */
    public function Find() : int {
        for( $x = 0; $x < $this->xDimension; $x++ ) {
            for( $y = 0; $y < $this->yDimension; $y++ ) {
                if( $this->grid[$y][$x] === $this->wordToFind[0] ) {
                    $this->searchCompassDirections($x, $y);
                }
            }
        }

        return count($this->occurrences);
    }

    /**
     * @param int $x
     * @param int $y
     */
    private function searchCompassDirections(int $x, int $y) {
        for( $index = 0; $index < 8; $index++ ) {
            $str = $this->getLettersInDirection($x, $y, $index);
            if( $str === $this->wordToFind ) {
                $this->occurrences[] = [$x, $y, self::DIRECTIONS[$index]];
            }
        }
    }

    /**
     * @param int $x
     * @param int $y
     * @param int $directionIndex
     *
     * @return string the string comprised of the letters found by starting at
     *                the given x,y and moving in the given direction.
     */
    private function getLettersInDirection(int $x, int $y, int $directionIndex) : string {
        if( $directionIndex > 7 ) {
            return '';
        }

        $str = '';
        $xDirection = self::DIRECTIONS[$directionIndex][0];
        $yDirection = self::DIRECTIONS[$directionIndex][1];

        // Check to see if the word can even fit at this spot.
        $endX = $x + ($this->wordLen * $xDirection);
        if( $endX < 0 || $endX > $this->xDimension ) {
            return '';
        }
        $endY = $y + ($this->wordLen * $yDirection);
        if( $endY < 0 || $endY > $this->yDimension ) {
            return '';
        }

        // Build the string from the letters encountered by following the given
        // direction.
        for( $i = 0; $i < $this->wordLen; $i++ ) {
            $str .= $this->grid[$y][$x];
            $x += $xDirection;
            $y += $yDirection;
        }

        return $str;
    }
}
