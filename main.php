<?php

use Jasonmm\DataRxChallenge\Commands\FindCommand;
use Symfony\Component\Console\Application;

require_once 'vendor/autoload.php';

$app = new Application('DataRx Challengen');
$app->add(new FindCommand());
$app->run();
